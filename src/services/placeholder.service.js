const baseUrl = "https://via.placeholder.com/";

export const getPlaceHolderUrl = (width, height) => {
    return `${baseUrl}${width}x${height}`;
};
