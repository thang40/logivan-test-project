import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { RootReducer, RootSaga } from "./ducks/combine";
import HomePage from "./pages/home/home.page";
import "./App.css";

const composeEnhancers =
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        : compose;

const enhancer = middleware => composeEnhancers(applyMiddleware(...middleware));

class App extends Component {
    constructor(props) {
        super(props);
        this.sagaMiddleware = createSagaMiddleware();
        this.store = createStore(RootReducer, enhancer([this.sagaMiddleware]));
        this.sagaMiddleware.run(RootSaga);
    }

    render() {
        return (
            <div className="App">
                <Provider store={this.store}>
                    <header />
                    <main>
                        <HomePage />
                    </main>
                </Provider>
            </div>
        );
    }
}

export default App;
