import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Form } from "react-bootstrap";
import { createForm, formShape } from "rc-form";
class DriverForm extends Component {
    id = {
        name: "driverId",
        label: "Id*",
        placeholder: "Id",
        options: {
            initialValue: "",
            rules: [
                {
                    required: true,
                    message: "Id is required"
                },
                {
                    type: "integer",
                    message: "Id must be a number",
                    transform: value => Number(value)
                },
                {
                    validator: (rule, value, callback) => {
                        const { onCheckId } = this.props;
                        onCheckId(Number(value), callback);
                    }
                }
            ]
        }
    };

    name = {
        name: "name",
        label: "Name*",
        placeholder: "Driver's name",
        options: {
            initialValue: "",
            rules: [{ required: true, message: "Name is required" }]
        }
    };

    address = {
        name: "address",
        label: "Addresss*",
        placeholder: "Address",
        options: {
            initialValue: "",
            rules: [{ required: true, message: "Address is required" }]
        }
    };

    handleSubmit = e => {
        e.preventDefault();
        const { form, onSubmit } = this.props;
        form.validateFields((error, value) => {
            if (!error) {
                onSubmit(value);
            }
        });
    };

    renderErrorSection = name => {
        const { getFieldError } = this.props.form;
        const errors = getFieldError(name);
        return errors
            ? errors.map((err, index) => {
                  return <div key={index}>{err}</div>;
              })
            : null;
    };

    render() {
        const { getFieldProps } = this.props.form;
        const { onCancel } = this.props;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group>
                    <Form.Label>{this.id.label}</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder={this.id.placeholder}
                        {...getFieldProps(this.id.name, this.id.options)}
                    />
                    <div>{this.renderErrorSection(this.id.name)}</div>
                </Form.Group>
                <Form.Group>
                    <Form.Label>{this.name.label}</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder={this.name.placeholder}
                        {...getFieldProps(this.name.name, this.name.options)}
                    />
                    <div>{this.renderErrorSection(this.name.name)}</div>
                </Form.Group>
                <Form.Group>
                    <Form.Label>{this.address.label}</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder={this.address.placeholder}
                        {...getFieldProps(this.address.name, this.address.options)}
                    />
                    <div>{this.renderErrorSection(this.address.name)}</div>
                </Form.Group>
                <Button className="mr-2" type="submit">
                    Save
                </Button>
                <Button variant="link" onClick={onCancel}>
                    Cancel
                </Button>
            </Form>
        );
    }
}

DriverForm.defaultProps = {
    onSubmit: () => {},
    onCancel: () => {},
    onCheckId: () => {},
    form: null
};

DriverForm.propTypes = {
    form: formShape,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    onCheckId: PropTypes.func
};

export const createDriverForm = options => {
    return createForm(options)(DriverForm);
};
