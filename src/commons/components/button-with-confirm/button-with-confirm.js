import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "react-bootstrap";

export class ButtonWithConfirm extends Component {
    state = {
        showConfirm: false
    };

    toggleConfirm = () => {
        this.setState({
            showConfirm: !this.state.showConfirm
        });
    };

    renderConfirm = (onConfirm, showText) => {
        return (
            <React.Fragment>
                {showText ? <div>Are you sure?</div> : null}
                <Button
                    size="sm"
                    className="mr-2"
                    onClick={() => {
                        onConfirm();
                        this.toggleConfirm();
                    }}
                >
                    Confirm
                </Button>
                <Button variant="link" size="sm" onClick={this.toggleConfirm}>
                    Cancel
                </Button>
            </React.Fragment>
        );
    };

    render() {
        const { onConfirm, children, showText } = this.props;
        const { showConfirm } = this.state;
        return showConfirm ? (
            this.renderConfirm(onConfirm, showText)
        ) : (
            <Button size="sm" className="mr-2" onClick={this.toggleConfirm}>
                {children}
            </Button>
        );
    }
}

ButtonWithConfirm.defaultProps = {
    onConfirm: () => {},
    showText: true,
    children: null
};

ButtonWithConfirm.propTypes = {
    onConfirm: PropTypes.func,
    showText: PropTypes.bool,
    children: PropTypes.node
};
