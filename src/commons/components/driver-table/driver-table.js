import React from "react";
import PropTypes from "prop-types";
import { Table } from "react-bootstrap";
import { createEditInlineInput } from "../edit-inline-input/edit-inline-input";
import { ButtonWithConfirm } from "../button-with-confirm/button-with-confirm";

export const DriverTable = ({ data, onDelete, onUpdate }) => {
    const handleDelete = id => {
        onDelete(id);
    };
    const onNameUpdate = (newName, oldData) => {
        const { driverId, address } = oldData;
        onUpdate({
            driverId,
            name: newName,
            address
        });
    };
    const onAddressUpdate = (newAddress, oldData) => {
        const { driverId, name } = oldData;
        onUpdate({
            driverId,
            name,
            address: newAddress
        });
    };
    const renderTableData = () => {
        return data.map((driver, index) => {
            const { driverId, name, address } = driver;
            const NameEditInline = createEditInlineInput("name", {
                initialValue: name,
                rules: [
                    {
                        required: true
                    }
                ]
            });
            const AddressEditInline = createEditInlineInput("address", {
                initialValue: address,
                rules: [
                    {
                        required: true
                    }
                ]
            });
            return (
                <tr key={driverId}>
                    <td>{driverId}</td>
                    <td>
                        <NameEditInline
                            value={name}
                            onSave={newName => onNameUpdate(newName, driver)}
                        />
                    </td>
                    <td>
                        <AddressEditInline
                            value={address}
                            onSave={newAddress => onAddressUpdate(newAddress, driver)}
                        />
                    </td>
                    <td>
                        <ButtonWithConfirm onConfirm={() => handleDelete(driverId)}>
                            Delete
                        </ButtonWithConfirm>
                    </td>
                </tr>
            );
        });
    };
    return (
        <Table bordered hover size="sm">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>{renderTableData()}</tbody>
        </Table>
    );
};

DriverTable.defaultProps = {
    driverData: {
        driverId: undefined,
        name: "",
        address: ""
    }
};

DriverTable.propTypes = {
    driverData: PropTypes.exact({
        driverId: PropTypes.number,
        name: PropTypes.string,
        address: PropTypes.string
    })
};
