import React, { Component } from "react";
import { createForm } from "rc-form";
import { Form, Button } from "react-bootstrap";

const enhanceEditInline = (name, validateOptions) => {
    return class extends Component {
        state = {
            isEdit: false
        };

        toggleMode = () => {
            const { value } = this.props;
            this.setState({
                isEdit: !this.state.isEdit,
                editValue: value || ""
            });
        };

        handleSave = () => {
            const { onSave } = this.props;
            const { form } = this.props;
            form.validateFields((error, value) => {
                if (!error) {
                    this.toggleMode();
                    onSave(value[name]);
                }
            });
        };

        renderErrorSection = name => {
            const { getFieldError } = this.props.form;
            const errors = getFieldError(name);
            return errors
                ? errors.map((err, index) => {
                      return <div key={index}>{err}</div>;
                  })
                : null;
        };

        renderEditMode = value => {
            const { getFieldProps } = this.props.form;
            return (
                <React.Fragment>
                    <Form.Control
                        type="text"
                        value={value}
                        {...getFieldProps(name, validateOptions)}
                    />
                    <div>{this.renderErrorSection(name)}</div>
                    <div className="mt-2">
                        <Button size="sm" className="mr-2" onClick={this.handleSave}>
                            Save
                        </Button>
                        <Button variant="link" size="sm" onClick={this.toggleMode}>
                            Cancel
                        </Button>
                    </div>
                </React.Fragment>
            );
        };

        renderReadOnlyMode = value => {
            return <span onClick={this.toggleMode}>{value}</span>;
        };

        render() {
            const { value } = this.props;
            const { isEdit } = this.state;
            return isEdit ? this.renderEditMode(value) : this.renderReadOnlyMode(value);
        }
    };
};

export const createEditInlineInput = (name, validateOptions) => {
    return createForm()(enhanceEditInline(name, validateOptions));
};
