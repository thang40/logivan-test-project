import React from "react";
import PropTypes from "prop-types";
import { Card } from "react-bootstrap";
import { createEditInlineInput } from "../edit-inline-input/edit-inline-input";
import { getPlaceHolderUrl } from "../../../services/placeholder.service";
import { ButtonWithConfirm } from "../button-with-confirm/button-with-confirm";

export const DriverCard = ({ driverData, onDelete, onUpdate }) => {
    const { driverId, name, address } = driverData;
    const NameEditInline = createEditInlineInput("name", {
        initialValue: name,
        rules: [
            {
                required: true
            }
        ]
    });
    const AddressEditInline = createEditInlineInput("address", {
        initialValue: address,
        rules: [
            {
                required: true
            }
        ]
    });
    const handleDelete = () => {
        onDelete(driverId);
    };
    const onNameUpdate = newName => {
        onUpdate({
            driverId,
            name: newName,
            address
        });
    };
    const onAddressUpdate = newAddress => {
        onUpdate({
            driverId,
            name,
            address: newAddress
        });
    };
    return (
        <Card style={{ width: "18rem" }} className="mb-4">
            <Card.Img variant="top" src={getPlaceHolderUrl(286, 180)} />
            <Card.Body>
                <Card.Title>
                    <NameEditInline value={name} onSave={onNameUpdate} />
                </Card.Title>

                <Card.Text>
                    <AddressEditInline value={address} onSave={onAddressUpdate} />
                </Card.Text>
                <ButtonWithConfirm onConfirm={handleDelete}>Delete</ButtonWithConfirm>
            </Card.Body>
        </Card>
    );
};

DriverCard.defaultProps = {
    driverData: {
        driverId: undefined,
        name: "",
        address: ""
    }
};

DriverCard.propTypes = {
    driverData: PropTypes.exact({
        driverId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        name: PropTypes.string,
        address: PropTypes.string
    })
};
