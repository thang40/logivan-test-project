import React, { Component } from "react";
import { connect } from "react-redux";
import {
    selectAllDriver,
    getDriverAction,
    createDriverAction,
    deleteDriverAction,
    updateDriverAction,
    resetDeleteErrorAction,
    checkIdWhenCreateAction,
    selectCurrentPage,
    selectTotalPage,
    selectDeleteError
} from "../../ducks/driver.duck";
import {
    Button,
    Container,
    Row,
    Col,
    Tabs,
    Tab,
    InputGroup,
    FormControl,
    Alert,
    Badge,
    OverlayTrigger,
    Tooltip
} from "react-bootstrap";
import {
    DriverCard,
    createDriverForm,
    DriverTable,
    ButtonWithConfirm
} from "../../commons/components";

const DriverForm = createDriverForm();
const tabNameWithToolTip = (name, tooltip) => (
    <div>
        <span>
            {`${name} `}
            <OverlayTrigger
                trigger="hover"
                placement="top"
                overlay={<Tooltip id="tooltip-disabled">{tooltip}</Tooltip>}
            >
                <Badge variant="info">i</Badge>
            </OverlayTrigger>
        </span>
    </div>
);

class HomePage extends Component {
    state = {
        isAddNew: false,
        deleteIdInputValue: ""
    };

    componentDidMount() {
        const { getDriverAction } = this.props;
        getDriverAction();
    }

    handleShowMoreDrivers = () => {
        const { getDriverAction, currentPage, totalPage } = this.props;
        if (currentPage < totalPage) {
            getDriverAction(currentPage + 1);
        }
    };

    handleOpenAddNew = () => {
        this.setState({
            isAddNew: true
        });
    };

    handleCloseAddNew = () => {
        this.setState({
            isAddNew: false
        });
    };

    handleCreateDriver = driverData => {
        const { createDriverAction } = this.props;
        const mappedDriverData = {
            ...driverData,
            driverId: Number(driverData.driverId)
        };
        createDriverAction(mappedDriverData);
        this.handleCloseAddNew();
    };

    handleDeleteDriver = id => {
        const { deleteDriverAction } = this.props;
        deleteDriverAction(Number(id));
    };

    handleUpdateDriver = driverData => {
        const { updateDriverAction } = this.props;
        updateDriverAction(driverData);
    };

    handleOnDelInputChange = e => {
        const { resetDeleteErrorAction } = this.props;
        this.setState({
            deleteIdInputValue: e.target.value
        });
        resetDeleteErrorAction();
    };

    renderAddNew = () => {
        const { checkIdWhenCreateAction } = this.props;
        const { isAddNew } = this.state;
        if (!isAddNew) {
            return;
        }
        return (
            <React.Fragment>
                <DriverForm
                    onSubmit={this.handleCreateDriver}
                    onCancel={this.handleCloseAddNew}
                    onCheckId={checkIdWhenCreateAction}
                />
            </React.Fragment>
        );
    };

    renderVanList = () => {
        const { drivers } = this.props;
        if (drivers.length === 0) {
            return;
        }
        return drivers.map(driver => {
            const { driverId } = driver;
            return (
                <Col key={driverId}>
                    <DriverCard
                        driverData={driver}
                        onDelete={this.handleDeleteDriver}
                        onUpdate={this.handleUpdateDriver}
                    />
                </Col>
            );
        });
    };

    render() {
        const { drivers, currentPage, totalPage, deleteIdError } = this.props;
        const { deleteIdInputValue } = this.state;
        return (
            <React.Fragment>
                <Container>
                    <h3>Driver list</h3>

                    <Row>
                        <Col lg={6}>
                            {deleteIdError ? (
                                <Alert variant="warning">{deleteIdError}</Alert>
                            ) : null}

                            <InputGroup className="mb-3">
                                <Button className="mr-4" onClick={this.handleOpenAddNew}>
                                    Add
                                </Button>
                                <FormControl
                                    placeholder="Driver id"
                                    value={deleteIdInputValue}
                                    onChange={this.handleOnDelInputChange}
                                />
                                <InputGroup.Append>
                                    <ButtonWithConfirm
                                        showText={false}
                                        onConfirm={() =>
                                            this.handleDeleteDriver(deleteIdInputValue)
                                        }
                                    >
                                        Delete
                                    </ButtonWithConfirm>
                                </InputGroup.Append>
                            </InputGroup>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col lg={4}>{this.renderAddNew()}</Col>
                    </Row>
                    <Row>
                        <Col>
                            <Tabs defaultActiveKey="card">
                                <Tab
                                    eventKey="card"
                                    title={tabNameWithToolTip(
                                        "Cards",
                                        "Click on name or address to edit"
                                    )}
                                >
                                    <Row>{this.renderVanList()}</Row>
                                </Tab>

                                <Tab
                                    eventKey="table"
                                    title={tabNameWithToolTip(
                                        "table",
                                        "Click on name or address to edit"
                                    )}
                                >
                                    <Row>
                                        <Col>
                                            <DriverTable
                                                data={drivers}
                                                onDelete={this.handleDeleteDriver}
                                                onUpdate={this.handleUpdateDriver}
                                            />
                                        </Col>
                                    </Row>
                                </Tab>
                            </Tabs>
                        </Col>
                    </Row>
                    {currentPage < totalPage ? (
                        <div className="mt-2">
                            <Button onClick={this.handleShowMoreDrivers}>Show more</Button>
                        </div>
                    ) : null}
                </Container>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        drivers: selectAllDriver(state),
        currentPage: selectCurrentPage(state),
        totalPage: selectTotalPage(state),
        deleteIdError: selectDeleteError(state)
    };
};

export default connect(
    mapStateToProps,
    {
        getDriverAction,
        createDriverAction,
        deleteDriverAction,
        updateDriverAction,
        resetDeleteErrorAction,
        checkIdWhenCreateAction
    }
)(HomePage);
