// rootReducer.js
import { combineReducers } from "redux";
import { all } from "redux-saga/effects";
import { DriverReducer, DriverSaga } from "./driver.duck.js";

// Use ES6 object literal shorthand syntax to define the object shape
export const RootReducer = combineReducers({
    DriverReducer
});

export function* RootSaga() {
    yield all([...DriverSaga]);
}
