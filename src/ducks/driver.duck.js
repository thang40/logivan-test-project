import { delay } from "redux-saga";
import { put, takeLatest, select } from "redux-saga/effects";

const DRIVER_GET_REQUEST = "@DRIVER/DRIVER_GET_REQUEST";
const DRIVER_GET_SUCCESS = "@DRIVER/DRIVER_GET_SUCCESS";
const DRIVER_CREATE_REQUEST = "@DRIVER/DRIVER_CREATE_REQUEST";
const DRIVER_CREATE_SUCCESS = "@DRIVER/DRIVER_CREATE_SUCCESS";
const DRIVER_UPDATE_REQUEST = "@DRIVER/DRIVER_UPDATE_REQUEST";
const DRIVER_UPDATE_SUCCESS = "@DRIVER/DRIVER_UPDATE_SUCCESS";
const DRIVER_DELETE_REQUEST = "@DRIVER/DRIVER_DELETE_REQUEST";
const DRIVER_DELETE_SUCCESS = "@DRIVER/DRIVER_DELETE_SUCCESS";
const DRIVER_DELETE_ERROR = "@DRIVER/DRIVER_DELETE_ERROR";
const DRIVER_RESET_DELETE_ERROR = "@DRIVER/DRIVER_RESET_DELETE_ERROR";
const DRIVER_CHECK_IF_ID_EXIST = "@DRIVER/DRIVER_CHECK_IF_ID_EXIST";

const initialState = {
    fakeDataEndPoint: [
        {
            driverId: 1,
            name: "Nguyen Van Thanh",
            address: "200 Nguyen Co Thach"
        },
        {
            driverId: 2,
            name: "Nguyen Thanh Nam",
            address: "215 Hoa Binh"
        },
        {
            driverId: 3,
            name: "Tran Khanh Nhan",
            address: "502 Pham The Hien"
        },
        {
            driverId: 4,
            name: "Nguyen Minh Nhat",
            address: "312 Xi Co Thach"
        },
        {
            driverId: 5,
            name: "Le Thanh Minh",
            address: "111 Hoa 3"
        },
        {
            driverId: 6,
            name: "Tran Minh Nhan",
            address: "23 The Hien"
        },
        {
            driverId: 7,
            name: "Bui Van Quang",
            address: "200 Minh Co Thach"
        },
        {
            driverId: 8,
            name: "Nguyen Quang Nam",
            address: "215 Hoa Binh"
        },
        {
            driverId: 9,
            name: "Tran Quan Nhan",
            address: "131 The Hien"
        }
    ],
    data: [],
    deleteError: undefined,
    currentPage: 1,
    totalPage: 3
};

export const getDriverAction = (page = 1) => ({
    type: DRIVER_GET_REQUEST,
    payload: { page }
});

export const createDriverAction = driverData => ({
    type: DRIVER_CREATE_REQUEST,
    payload: driverData
});

export const updateDriverAction = driverData => ({
    type: DRIVER_UPDATE_REQUEST,
    payload: driverData
});

export const deleteDriverAction = id => ({
    type: DRIVER_DELETE_REQUEST,
    payload: id
});

export const resetDeleteErrorAction = () => ({
    type: DRIVER_RESET_DELETE_ERROR
});

export const checkIdWhenCreateAction = (id, callback) => ({
    type: DRIVER_CHECK_IF_ID_EXIST,
    payload: { id, callback }
});

export const DriverReducer = (state = initialState, action) => {
    switch (action.type) {
        case DRIVER_GET_SUCCESS: {
            const { currentPage } = action.payload;
            return {
                ...state,
                data: [
                    ...state.data,
                    ...state.fakeDataEndPoint.slice(
                        3 * (currentPage - 1),
                        3 * (currentPage - 1) + 3
                    )
                ],
                currentPage: currentPage
            };
        }
        case DRIVER_CREATE_SUCCESS: {
            const newFakeEndPoint = [...state.fakeDataEndPoint, action.payload];
            const totalItem = newFakeEndPoint.length;
            const newTotalPage = Math.ceil(totalItem / 3);
            return {
                ...state,
                fakeDataEndPoint: newFakeEndPoint,
                data: [...newFakeEndPoint.slice(0, 3)],
                currentPage: 1,
                totalPage: newTotalPage
            };
        }
        case DRIVER_UPDATE_SUCCESS: {
            const updatedDriver = action.payload;
            const driverIndex = state.data.findIndex(
                driver => driver.driverId === updatedDriver.driverId
            );
            if (driverIndex !== -1) {
                const updatedData = [...state.data];
                updatedData[driverIndex] = updatedDriver;
                return { ...state, data: updatedData };
            }
            return { ...state };
        }

        case DRIVER_DELETE_SUCCESS: {
            const id = action.payload;
            const driverIndex = state.fakeDataEndPoint.findIndex(driver => driver.driverId === id);
            if (driverIndex !== -1) {
                const updatedFakeEndPoint = [...state.fakeDataEndPoint];
                updatedFakeEndPoint.splice(driverIndex, 1);
                const totalItem = updatedFakeEndPoint.length;
                const newTotalPage = Math.ceil(totalItem / 3);
                return {
                    ...state,
                    fakeDataEndPoint: updatedFakeEndPoint,
                    data: [...updatedFakeEndPoint.slice(0, 3)],
                    currentPage: 1,
                    totalPage: newTotalPage
                };
            }
            return { ...state };
        }
        case DRIVER_DELETE_ERROR:
            return { ...state, deleteError: action.payload };

        case DRIVER_RESET_DELETE_ERROR:
            return { ...state, deleteError: undefined };
        default:
            return state;
    }
};

//selector
export const selectAllDriver = state => state.DriverReducer.data;
export const selectCurrentPage = state => state.DriverReducer.currentPage;
export const selectTotalPage = state => state.DriverReducer.totalPage;
export const selectDeleteError = state => state.DriverReducer.deleteError;
const selectFakeDataEndPoint = state => state.DriverReducer.fakeDataEndPoint;
//side effect
function* watchGetDriver(action) {
    try {
        yield delay(500);
        const { page } = action.payload;
        yield put({
            type: DRIVER_GET_SUCCESS,
            payload: { currentPage: page }
        });
    } catch (error) {}
}

function* watchCreateDriver(action) {
    try {
        yield delay(500);
        const driverData = action.payload;
        yield put({
            type: DRIVER_CREATE_SUCCESS,
            payload: driverData
        });
    } catch (error) {}
}

function* watchUpdateDriver(action) {
    try {
        yield delay(500);
        yield put({
            type: DRIVER_UPDATE_SUCCESS,
            payload: action.payload
        });
    } catch (error) {}
}

function* watchDeleteDriver(action) {
    try {
        const id = action.payload;
        const fakeDataEndPoint = yield select(selectFakeDataEndPoint);
        if (fakeDataEndPoint.findIndex(driver => driver.driverId === id) === -1) {
            throw new Error();
        }
        yield put({
            type: DRIVER_DELETE_SUCCESS,
            payload: id
        });
    } catch (error) {
        yield put({
            type: DRIVER_DELETE_ERROR,
            payload: "Id does not exist"
        });
    }
}

function* watchCheckIdExists(action) {
    try {
        yield delay(1000);
        const id = action.payload.id;
        const callback = action.payload.callback;
        const fakeDataEndPoint = yield select(selectFakeDataEndPoint);
        const isIdExist = fakeDataEndPoint.findIndex(driver => driver.driverId === id) !== -1;
        if (isIdExist) {
            callback("Id already exists");
        } else {
            callback();
        }
    } catch (error) {}
}

export const DriverSaga = [
    takeLatest(DRIVER_GET_REQUEST, watchGetDriver),
    takeLatest(DRIVER_CREATE_REQUEST, watchCreateDriver),
    takeLatest(DRIVER_UPDATE_REQUEST, watchUpdateDriver),
    takeLatest(DRIVER_DELETE_REQUEST, watchDeleteDriver),
    takeLatest(DRIVER_CHECK_IF_ID_EXIST, watchCheckIdExists)
];
