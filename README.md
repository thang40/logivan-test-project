## Info

Example of React + Redux + Redux-Saga app which manage all the truck drivers.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Techs

-   rc-form: ^2.3.0
-   react: ^16.6.3
-   react-bootstrap: "1.0.0-beta.3
-   react-dom: ^16.6.3
-   react-redux: ^6.0.0
-   react-scripts: 2.1.1
-   redux: ^4.0.1"
-   redux-saga: ^0.16.2

## How to run

1. `yarn install` or `npm install`
2. `yarn start` or `npm start`
